import "./App.css";
import "./index.css";
import { Emoji } from "emoji-picker-react";

function App() {
  const skills = [
    {
      skillsOnCard: "ballet",
      colorsOfSkills: "yellow",
      type: "classical",
    },
    {
      skillsOnCard: "hiphop",
      colorsOfSkills: "lawngreen",
      type: "modern",
    },
    {
      skillsOnCard: "salsa",
      colorsOfSkills: "coral",
      type: "latin",
    },
    {
      skillsOnCard: "street",
      colorsOfSkills: "dodgerblue",
      type: "modern",
    },
    {
      skillsOnCard: "rumba",
      colorsOfSkills: "red",
      type: "latin",
    },
  ];

  const introText = `Lorem ipsum dolor sit amet, 
      consectetur adipiscing elit. 
      Tum ille timide vel potius verecunde: 
      Facio, inquit.Lorem ipsum dolor sit amet, 
      consectetur adipiscing elit. 
      Tum ille timide vel potius verecunde: 
      Facio, inquit. Photo: Budgeron Bach, Pexels`;

  return (
    <div className="card">
      <Avatar
        className="avatar"
        photo="./assets/pexels-budgeron-bach-5150510.jpg"
        name="Photo by Budgeron Bach"
      />
      <div className="data">
        <Intro text={introText} />
        <SkillList skills={skills} />
      </div>
    </div>
  );
}

function Avatar({ photo, name }) {
  return (
    <div>
      <img src={photo} alt={name} />
    </div>
  );
}

function Intro({ text }) {
  return (
    <div className="intro">
      <h1>Bianca Neve</h1>
      {text}
    </div>
  );
}

function SkillList({ skills }) {
  const skillsForCard = skills.map((skill) => (
    <Skill key={skill.skillsOnCard} skillObject={skill} />
  ));

  return <div className="skill-list">{skillsForCard}</div>;
}

function Skill({ skillObject }) {
  return (
    <div
      className="skill"
      style={{ backgroundColor: skillObject.colorsOfSkills }}
    >
      {skillObject.skillsOnCard}
      {skillObject.type === "classical" && <Emoji unified="1f938" size="15" />}
      {skillObject.type === "latin" && <Emoji unified="1f483" size="15" />}
      {skillObject.type === "modern" && <Emoji unified="1f57a" size="15" />}
    </div>
  );
}
export default App;
