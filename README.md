# Business Card Demo

A small demo app created in ReactJS. Businesscard demo with re-usable components

## Screenshots

![Business-card-demo](./public/assets/demopic1.png)

## Author

Katlin Kalde

- index.css initial base was taken from https://www.udemy.com/course/the-ultimate-react-course and adapted later.

- Photo: Pexels, Budgeron Bach

## Rights

Free to use as you like, no attribution needed. For the photo rights see Pexels licence policy.
